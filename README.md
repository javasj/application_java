
PROJET : Conversion d'un automate fini non deterministe en automate fini deterministe
LANGAGE : JAVA
GROUPE : ELHADI Jihad - QANNOUF Safae
NB : Nous avons utilis� la biblioth�que JGraphX pour dessiner les automates
ETAPE A FAIRE : 
 1- Configurer du PATH de JGraphX dans le projet
 2- Lancer de l'application
 3- Faire entrer l'alphabet
 4- Faire entrer le nombres des �tats
 5- Choisir les �tats finaux
 6- Remplir la fonction de transition de l'AFN
 7- Conversion